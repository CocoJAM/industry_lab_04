package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public String computer_name = "Computer";
    public int player_choice;
    public int computer_choice;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        System.out.print("Hi! What is your name? ");
        String name = Keyboard.readInput();
        while (player_choice != 4) {
            System.out.println();
            System.out.println("1. Rock\n2. Scissors\n3. Paper\n4. Quit\n     Enter choice: ");
            int choice = Integer.parseInt(Keyboard.readInput());
            System.out.println();
            computer_choice= (int) (Math.random()*3+1);
            if(choice == 4){
                System.out.print("Goodbye "+ name + ". Thanks for playing.");
                break;
            }
            if (choice != 4) {
                displayPlayerChoice(name, choice);
                displayPlayerChoice(computer_name, computer_choice);
            } else {
                displayPlayerChoice(name, choice);
            }
            if (userWins(choice, computer_choice)) {
                System.out.println(name + " wins because " + getResultString(choice, computer_choice));
            } else if (choice == computer_choice) {
                System.out.println("No one wins.");
            } else {
                System.out.println(computer_name + " wins because " + getResultString(computer_choice, choice));
            }


            // as detailed in the exercise sheet.
        }
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        if (choice == 1){
            System.out.println(name + " chose rock.");
        }
        else if (choice == 2 ){
            System.out.println(name + " chose scissors.");
        }
        else if (choice == 3){
            System.out.println(name + " chose paper.");
        }

    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice%3+1 == computerChoice%3){
            return true;
        }
        else {
            return false;
        }
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock.";
        final String ROCK_WINS = "rock smashes scissors.";
        final String SCISSORS_WINS = "scissors cut paper.";
        final String TIE = " you chose the same as the computer.";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == 3 && computerChoice == 1){
            return PAPER_WINS;
        }
        else if (playerChoice== 1 && computerChoice == 2){
            return ROCK_WINS;
        }
        else if (playerChoice == 2 && computerChoice == 1){
            return SCISSORS_WINS;
        }
        else {
            return TIE;
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
