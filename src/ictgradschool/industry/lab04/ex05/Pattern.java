package ictgradschool.industry.lab04.ex05;

/**
 * Created by ljam763 on 14/03/2017.
 */
public class Pattern {
    private char symbol;
    private int repeitition;
    private int numberofcharacters;
    public Pattern (int repeitition,char symbol ){
        this.repeitition = repeitition;
        this.symbol = symbol;
    }
    public void setNumberOfCharacters (int repeitition){
        this.repeitition = repeitition;
    }
    public int getNumberOfCharacters (){
        return repeitition;
    }
    public String toString(){
        String target = "";
        for (int i = 0; i< repeitition;i++){
            target += symbol;
        }
        return target;
    }

}
