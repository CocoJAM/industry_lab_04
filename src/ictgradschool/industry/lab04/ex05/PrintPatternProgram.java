package ictgradschool.industry.lab04.ex05;

public class PrintPatternProgram {

    public void start() {
        
        printPatternOne();
        printPatternTwo();
        
    }
    private void printPatternOne() {
        System.out.println("First Pattern");

        // TODO Uncomment this code once you've created the Pattern class.

        Pattern top = new Pattern(15, '*');
        
        Pattern sideOfFirstLine = new Pattern(7, '#');
        Pattern sideOfLine = new Pattern(7, '~');
        Pattern middle = new Pattern(1, '.');
        
        System.out.println(top);
        System.out.println(sideOfFirstLine.toString() + middle.toString() + sideOfFirstLine.toString());
        
        for (int i = 0; i < 6; i++) {
            middle.setNumberOfCharacters(middle.getNumberOfCharacters() + 1);
            System.out.println(sideOfLine.toString() + middle.toString() + sideOfLine.toString());
        }
        
        System.out.println();
    }
    
    private void printPatternTwo() {
        System.out.println("Second Pattern");
        // You complete the code to produce the second pattern
        Pattern top2 = new Pattern(36, '@');
        Pattern sideOfFirstLine2 = new Pattern(12, '=');
        Pattern sideOfLine2 = new Pattern(12, '&');
        Pattern middle2 = new Pattern(12, '.');

        System.out.println(top2);
        System.out.println(sideOfFirstLine2.toString() + middle2.toString() + sideOfFirstLine2.toString());
        for (int i =0 ; i<6; i++){
            middle2.setNumberOfCharacters(middle2.getNumberOfCharacters() - 2);
            sideOfLine2.setNumberOfCharacters(sideOfLine2.getNumberOfCharacters()+1);
            System.out.println(sideOfLine2.toString() + middle2.toString() + sideOfLine2.toString());
        }

    }

    public static void main(String[] args) {
        PrintPatternProgram ppp = new PrintPatternProgram();
        ppp.start();
    }
}
